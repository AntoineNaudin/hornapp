#include "DrawArea.hpp"
#include <cairomm/context.h>
#include <cmath>

DrawArea::DrawArea():
  cote(5),
  longueur(100),
  angle(0.2)
{
  
}

DrawArea::~DrawArea()
{
}
void DrawArea::setCote(int c){
  cote = c;
  redessiner();
}
void DrawArea::setAngle(int a){
  angle = toRadian(a);
  redessiner();
}
void DrawArea::setLongueur(int l){
  longueur = l;
  redessiner();
}
float DrawArea::toRadian(int angle_degre){
  return angle_degre*M_PI/180;
  
}

void DrawArea::dessinerArcDeCercle(const Cairo::RefPtr<Cairo::Context>& cr, int N)
{
  int longueur_int = N * cote ;


  for(int i=1;i<=N;i++){
    cr->arc(0, 0, cote*i,M_PI/2-angle,M_PI/2);
    cr->stroke();  
  }

  cr->move_to(0,0); 
  cr->line_to(longueur_int*sin(angle),longueur_int*cos(angle));
  cr->stroke();

    
}


void DrawArea::dessinerCarre(const Cairo::RefPtr<Cairo::Context>& cr, int x0,int y0)
{
  /* 
  cr->set_source_rgba(0.2, 0, 0.5, 0.8); //violet
  cr->rectangle(x0, y0, cote, cote);
  cr->fill();
  */
  int x=x0;
  int y=y0;
  int i=0;
  int N = longueur / cote;
  int couleur=0;
  for(i =0;i < N ;i++){
    cr->set_source_rgba(0.313, 0.612, 0.117, 0.9);   // green 

    cr->save();
    cr->move_to(x,y);
    cr->line_to (x,y+cote);
    cr->line_to (x+cote,y+cote);
    cr->line_to (x+cote,y);
    cr->line_to (x,y);
    y = y+cote;
    cr->stroke();
    cr->restore();
   }
  // cr->line_to (x,y);

  cr->stroke();
   
  
}
void DrawArea:: redessiner(){
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();
  const int lesser = 3;// MIN(width, height);
  
  Cairo::RefPtr<Cairo::Context> cr = this->get_window()->create_cairo_context();
  
  // coordinates for the center of the window
  int xc, yc;
  xc = width / 2 - cote/2;
  yc = height / 2- (longueur/2);
  cr->set_line_width(lesser);// * 0.007);  // outline thickness changes
                                      // with window size
  queue_draw();

  dessinerCarre(cr,xc,yc);
  int N = longueur/cote;
  int dx=xc+cote;
  int dy=yc;
  for(int i=0;i<N;i++){
    cr->translate(dx, dy);// make (ex, ey) == (0, 0)
    dessinerArcDeCercle(cr,N-i);
    dx = cote*sin(angle);
    dy = cote*cos(angle);
    cr->rotate(-1* angle);
  }
}

bool DrawArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
  redessiner();  
  return true;
}
