#ifndef DEF_FENETRE
#define DEF_FENETRE

#include <iostream>
using namespace std;

#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gtkmm/notebook.h>
#include <gtkmm/stock.h>
#include <gtkmm/window.h>
#include <gtkmm/table.h>

#include <gtkmm/adjustment.h>
#include <gtkmm/spinbutton.h>


#include "DrawArea.hpp"

class Fenetre : public Gtk::Window {
public :
  Fenetre();
  void modifierTitre(Gtk::Widget* page, int numPage);
  void nouvelOnglet();
  void supprimerOnglet();
  void modifierAngle();
  void modifierCote();
  void modifierLongueur();
  void validerParametres();
        
private :
  Gtk::Notebook barreOnglets;
  Gtk::HBox boitePrincipale;
  Gtk::VBox sideBar; 
  Gtk::Button boutonNouvelOnglet;
  Gtk::Button boutonSupprimerOnglet;
  Gtk::Button boutonValider;
  Gtk::Image imageNouveau;
  Gtk::Image imageSupprimer;
  Gtk::Label angle;
  Gtk::Label cote;
  Gtk::Label longueur;
  Gtk::Label param;
  Gtk::Label page1;
  Glib::RefPtr<Gtk::Adjustment> ajustementAngle; //Ajustement.
  Glib::RefPtr<Gtk::Adjustment> ajustementCote; //Ajustement.
  Glib::RefPtr<Gtk::Adjustment> ajustementLongueur; //Ajustement.

  Gtk::SpinButton boutonAngle; //Bouton compteur.
  Gtk::SpinButton boutonCote; //Bouton compteur.
  Gtk::SpinButton boutonLongueur; //Bouton compteur.

  DrawArea* areas[1000];
   
  
};

#endif
