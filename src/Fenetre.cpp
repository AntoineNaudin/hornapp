#include "Fenetre.hpp"
#include "DrawArea.hpp"

Fenetre::Fenetre() :
  boutonValider("Valider !"),
  imageNouveau(Gtk::Stock::NEW, Gtk::ICON_SIZE_BUTTON),
  imageSupprimer(Gtk::Stock::DELETE, Gtk::ICON_SIZE_BUTTON),
  ajustementAngle(Gtk::Adjustment::create(1, 0, 90, 1)),
  ajustementCote(Gtk::Adjustment::create(5, 0, 10000, 1)),
  ajustementLongueur(Gtk::Adjustment::create(100, 0,10000000, 5)),
  boutonAngle(ajustementAngle),
  boutonCote(ajustementCote),
  boutonLongueur(ajustementLongueur){
  
  add(boitePrincipale);
  boitePrincipale.pack_start(sideBar,Gtk::PACK_SHRINK);
  boitePrincipale.pack_start(barreOnglets);
  
  Gtk::Label* label = Gtk::manage(new Gtk::Label());
  label->set_text("Angle (en °)");
  sideBar.pack_start(*label,Gtk::PACK_SHRINK);
  sideBar.pack_start(boutonAngle,Gtk::PACK_SHRINK); 
  boutonAngle.set_numeric();
  boutonAngle.signal_value_changed().connect(sigc::mem_fun(*this, &Fenetre::modifierAngle));

  Gtk::Label* label2 = Gtk::manage(new Gtk::Label());
  label2->set_text("Côté (en mm)");
  sideBar.pack_start(*label2,Gtk::PACK_SHRINK);
  sideBar.pack_start(boutonCote,Gtk::PACK_SHRINK); 
  boutonCote.set_numeric();
  boutonCote.signal_value_changed().connect(sigc::mem_fun(*this, &Fenetre::modifierCote));

  Gtk::Label* label3 = Gtk::manage(new Gtk::Label());
  label3->set_text("Longueur (en mm)");
  sideBar.pack_start(*label3,Gtk::PACK_SHRINK);
  sideBar.pack_start(boutonLongueur,Gtk::PACK_SHRINK); 
  boutonLongueur.set_numeric();
  boutonLongueur.signal_value_changed().connect(sigc::mem_fun(*this, &Fenetre::modifierLongueur));

  sideBar.pack_start(boutonValider,Gtk::PACK_SHRINK); 
  boutonValider.set_can_focus(false);
  boutonValider.show();

 
  sideBar.pack_start(param);
    
  /*
   * Création de la barre d'onglets
   * */

  //S'il y a trop d'onglets, nous pouvons naviguer à l'aide de flèche.
  barreOnglets.set_scrollable();
  //Ajouter un menu contextuel lors du clic-droit pour la navigation.
  barreOnglets.popup_enable();
  //Empêcher la barre d'avoir le focus.
  barreOnglets.set_can_focus(false);

  //Définir le groupe de la barre d'onglets (pour pouvoir échanger des onglets avec d'autres barre d'onglets).
  barreOnglets.set_group_name("onglets");

  //Ajout d'une page
  areas[0] = Gtk::manage(new DrawArea());
  barreOnglets.append_page(*areas[0]);
  barreOnglets.set_tab_reorderable(*areas[0]); //Cet onglet peut être réordonné.
  barreOnglets.set_tab_detachable(*areas[0]); //Cet onglet peut être détaché.
    
  /*
   * Création d'un bouton d'action.
   * */
    
  boutonNouvelOnglet.set_can_focus(false);
  boutonNouvelOnglet.set_image(imageNouveau); //Modifier l'image du bouton.

  boutonSupprimerOnglet.set_can_focus(false);
  boutonSupprimerOnglet.set_image(imageSupprimer); //Modifier l'image du bouton.

  //Ajout d'un widget d'action à la fin de la barre d'onglets.
  barreOnglets.set_action_widget(&boutonNouvelOnglet, Gtk::PACK_END);

  //Ajout d'un widget d'action au début de la barre d'onglets.
  barreOnglets.set_action_widget(&boutonSupprimerOnglet, Gtk::PACK_START);

  boutonNouvelOnglet.show(); //Il ne faut pas oublier de l'afficher manuellement.
  boutonSupprimerOnglet.show();
  barreOnglets.show();
    

  //Connexions
  boutonValider.signal_clicked().connect(sigc::mem_fun(*this, &Fenetre::validerParametres));
  boutonNouvelOnglet.signal_clicked().connect(sigc::mem_fun(*this, &Fenetre::nouvelOnglet));
  boutonSupprimerOnglet.signal_clicked().connect(sigc::mem_fun(*this, &Fenetre::supprimerOnglet));
  barreOnglets.signal_switch_page().connect(sigc::mem_fun(*this, &Fenetre::modifierTitre));

  show_all();
}

int pageCourante=0;
int totalPages=1;

void Fenetre::modifierTitre(Gtk::Widget* page, int numPage) {
  //Modifier le titre de la fenêtre selon l'onglet choisi.
  //set_title(barreOnglets.get_tab_label_text(*barreOnglets.get_nth_page(numPage)));
  pageCourante= numPage;
    
}
void Fenetre::supprimerOnglet() {
  barreOnglets.remove_page(pageCourante);
  totalPages --;
  show_all();
}


void Fenetre::nouvelOnglet() {
  areas[totalPages] = Gtk::manage(new DrawArea());
  barreOnglets.append_page(*areas[totalPages]);
  barreOnglets.set_tab_reorderable(*areas[totalPages]); 
  barreOnglets.set_tab_detachable(*areas[totalPages]); 
  show_all();
  totalPages++;
}

void Fenetre::modifierAngle(){
  cout << "page courante "<< pageCourante<<endl;
  areas[pageCourante]->setAngle(boutonAngle.get_value_as_int());
}

void Fenetre::modifierCote(){
  //cote.set_text(boutonCote.get_text());
  areas[pageCourante]->setCote(boutonCote.get_value_as_int());
}

void Fenetre::modifierLongueur(){
  areas[pageCourante]->setLongueur(boutonLongueur.get_value_as_int());
}

void Fenetre::validerParametres(){
  page1.set_text(
		 angle.get_text()
		 + " " + cote.get_text()
		 + " " + longueur.get_text()
		 );
  // area.setCote(boutonCote.get_value_as_int());

  
}
