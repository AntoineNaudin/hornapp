#ifndef GTKMM_DRAWAREA_H
#define GTKMM_DRAWAREA_H

#include <gtkmm/drawingarea.h>
#include <math.h>
class DrawArea : public Gtk::DrawingArea
{
public:
  DrawArea();
  virtual ~DrawArea();
  int cote;
  float angle ;// en radian;
  int longueur;
  float toRadian(int angle_degre);
  void dessinerCarre(const Cairo::RefPtr<Cairo::Context>& cr, int x0,int y0);
  void dessinerArcDeCercle(const Cairo::RefPtr<Cairo::Context>& cr, int N);
  void setCote(int c);
  void setLongueur(int c);
  void setAngle(int c);
  void redessiner();
protected:
  //Override default signal handler:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
};

#endif // GTKMM_DRAWAREA_H
